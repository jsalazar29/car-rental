<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('car_id');
            $table->string('name');
            $table->integer('brand_id')->unsigned()->nullable(false);
            $table->foreign('brand_id')
                ->references('brand_id')
                ->on('car_brands')
                ->onDelete('cascade');
            $table->integer('type_id')->unsigned()->nullable(false);
            $table->foreign('type_id')
                ->references('type_id')
                ->on('car_types')
                ->onDelete('cascade');
            $table->integer('quantity');
            $table->integer('rent');
            $table->boolean('is_available');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
