<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rentals', function (Blueprint $table) {
            $table->increments('rental_id')->nullable(false);
            $table->integer('user_id')->unsigned()->nullable(false);
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->integer('car_id')->unsigned()->nullable(false);
            $table->foreign('car_id')->references('car_id')->on('cars')->onDelete('cascade');
            $table->date('from');
            $table->date('to');
            $table->bigInteger('payment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rentals');
    }
}