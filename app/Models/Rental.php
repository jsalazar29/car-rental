<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
    use HasFactory;

    protected $table = "rentals";

    protected $primaryKey = "rental_id";

    protected $fillable = ["user_id", "car_id", "from", "to", "payment"];

    public function getUser()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function getCar()
    {
        return $this->belongsTo('App\Models\Car', 'car_id');
    }
}
