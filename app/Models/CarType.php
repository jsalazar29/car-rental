<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarType extends Model
{
    use HasFactory;

    protected $table = "car_types";

    protected $primaryKey = "type_id";

    protected $fillable = ["type_name"];

    public function getCars()
    {
        return $this->hasMany('App\Models\Car', 'type_id');    
    }
}
