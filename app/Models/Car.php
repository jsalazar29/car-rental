<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    protected $table = "cars";

    protected $primaryKey = "car_id";

    protected $fillable = ["name", "brand_id", "type_id", "quantity", "rent", "is_available", "photo_id"];

    public function getCarBrand()
    {
        return $this->belongsTo('App\Models\CarBrand', 'brand_id');
    }

    public function getCarType()
    {
        return $this->belongsTo('App\Models\CarType', 'type_id');
    }

    public function getPicture()
    {
        return $this->hasOne('App\Models\CarPicture', 'picture_id', 'photo_id');
    }
}
