<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarBrand extends Model
{
    use HasFactory;
    protected $table = "car_brands";

    protected $primaryKey = "brand_id";

    protected $fillable = ["brand_name"];

    public function getCars()
    {
        return $this->hasMany('App\Models\Car', 'brand_id');    
    }

}
