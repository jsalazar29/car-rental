<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Car, CarBrand, CarType, CarPicture};

class CarController extends Controller
{
    public function getCars()
    {
        $cars = Car::with([
            'getCarBrand',
            'getCarType',
            'getPicture'
        ])->get();      

        return response()->json($cars);
    }

    public function addCar(Request $request)
    {
        $car_photo = new CarPicture();

        if($request->file()) {
            $file_name = time().'_'.$request->file->getClientOriginalName();
            $file_path = $request->file('file')->storeAs('uploads', $file_name, 'public');

            $car_photo->name = time().'_'.$request->file->getClientOriginalName();
            $car_photo->path = '/storage/' . $file_path;
            $car_photo->save();
        }
        $car = $request->except(['file']);
        $car['photo_id'] = $car_photo->id;
        $car = Car::create($car);

        return response()->json(Car::with([
            'getCarBrand',
            'getCarType'
        ])->get());
    }

    public function getBrands()
    {
        return response()->json(CarBrand::all());
    }

    public function addBrand(Request $request)
    {
        CarBrand::create($request->all());

        return response()->json(['success' => true, 'message' => 'Successfully added a car brand']);
    }

    public function getTypes()
    {
        return response()->json(CarType::all());
    }

    public function addType(Request $request)
    {
        CarType::create($request->all());

        return response()->json(['success' => true, 'message' => 'Successfully added a car type']);
    }

}
