<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Http\Request;
use App\Models\Rental;

class RentalController extends Controller
{
    public function addRental(Request $request)
    {
        $rental = Rental::create($request->all());

        $car = Car::find($rental->car_id);
        $car->quantity = $car->quantity - 1;
        $car->save();

        return response()->json([
            'success' => true, 
            'message' => 'Successfully added a car brand', 
            'data' => $rental
        ]);
    }

    public function getRentals()
    {
        $rentals = Rental::with([
            'getUser',
            'getCar',
            'getCar.getCarBrand',
            'getCar.getCarType'
        ])->get();      

        return response()->json($rentals);
    }

    public function getUserRental($rental_id)
    {
        return response()->json(Rental::with([
                'getUser',
                'getCar',
                'getCar.getCarBrand',
                'getCar.getCarType'
            ])
            ->get()
            ->where('rental_id', $rental_id)
            ->first()
        );
    }
    
    public function editRental(Request $request, $rental_id)
    {
        $rental = Rental::where("rental_id", $rental_id)->update($request->except(['name']));

        return response()->json(['message' => 'Updated Successfully', 'success' => true, 'data' => $rental]);
    }

    public function delete($rental_id) 
    {
        $rental = Rental::find($rental_id);

        $rental->delete();

        return response()->json(Rental::with([
            'getUser',
            'getCar',
            'getCar.getCarBrand',
            'getCar.getCarType'
        ])->get());
    }
}
