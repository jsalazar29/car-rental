<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function store(Request $request)
    {
        $user = User::create($request->all());

        return $user;
    }

    public function getUsers() 
    {
        return response()->json(User::all()); 
    }

    public function delete($user_id) 
    {
        $user = User::find($user_id);

        $user->delete();

        return response()->json(User::all());
    }
}
