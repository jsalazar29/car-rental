<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarController;
use App\Http\Controllers\RentalController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->group(function () {
    Route::group(['prefix' => 'cars'], function () {
        Route::get('', [CarController::class, 'getCars']);
        Route::get('get-brands', [CarController::class, 'getBrands']);
        Route::get('get-types', [CarController::class, 'getTypes']);
        Route::post('add-car', [CarController::class, 'addCar']);
        Route::post('add-brand', [CarController::class, 'addBrand']);
        Route::post('add-type', [CarController::class, 'addType']);
    });
    Route::post('user', [UserController::class, 'store']);
    Route::delete('delete-user/{user_id}', [UserController::class, 'delete']);
    Route::get('get-user', [UserController::class, 'getUsers']);
    Route::group(['prefix' => 'rentals'], function () {
        Route::get('', [RentalController::class, 'getRentals']);
        Route::get('user-rental/{rental_id}', [RentalController::class, 'getUserRental']);
        Route::post('add-rental', [RentalController::class, 'addRental']);
        Route::put('edit-rental/{rental_id}', [RentalController::class, 'editRental']);
        Route::delete('delete-rental/{rental_id}', [RentalController::class, 'delete']);
    });
});
